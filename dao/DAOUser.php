<?php

/**
 * Cette classe me permet d'interagir avec ma table Adherent de ma BD Blackbooks
 * Je suis obligée d'implémenter les méthodes abstraites issues des interfaces de la class mère DAO
 * Dans les tests je vais instancier cet objet pour effectuer le CRUD sur ma table Adherent
 */
class DAOUser extends DAO
{


    // cette méthode permet de récupérer tous les users/adherents
    public function getAll()
    {
        $pdoStatement = $this->pdo->query("SELECT * from Adherent");
        return $pdoStatement->fetchAll();
    }

    // cette méthode sera invoquée avec des filtres
    public function getAllBy(array $filters)
    {
        $pdoStatement = $this->pdo->query("SELECT * from Adherent where ");
        return $pdoStatement->fetch();
    }


    public function create(array $data) 
    {
        // je stocke dans une variable le tableau des données qui seront créées dans la BD
        $donnees = array(
            ':nom' => $data['nom'],
            ':prenom' => $data['prenom'],
            ':email' => $data['email'],
            ':date_inscription' => $data['date_inscription'],
        );
        // je prépare ma requete "insert into" que je stocke dans une variable $pdoStatement
        $pdoStatement = $this->pdo->prepare("INSERT INTO Adherent (nom, prenom, email, date_inscription)
                                                VALUES (:nom, :prenom, :email, :date_inscription)");
        // avec la methode execute j'execute ma requete en donnant comme argument mon tableau de données
        $pdoStatement->execute($donnees);
    }


    public function retrieve(int $id) :array
    {
        // la méthode query sur l'objet PDO retourne un objet PDOStatement
        $pdoStatement = $this->pdo->query("SELECT * from Adherent where numero=" . $id);
        // pour récup l'adherent choisi, j'invoque la methode fetch sur l'objet PDOStatement
        $adh = $pdoStatement->fetch();
        // retourne l'adherent récupéré en BD
        return $adh;
    }


    public function update(array $data)
    {
        // je stocke dans une variable le tableau des données qui seront MAJ dans la BD
        $donnees = array(
            ':nom' => $data['nom'],
            ':prenom' => $data['prenom'],
            ':email' => $data['email'],
            ':date_inscription' => $data['date_inscription'],
        );
        // je prépare ma requete "update" que je stocke dans une variable $pdoStatement
        $pdoStatement = $this->pdo->prepare("UPDATE Adherent SET nom=:nom, prenom=:prenom, email=:email, date_inscription=:date_inscription WHERE numero=:numero");
        // avec la méthode execute j'execute la requete en donnant comme argument mon tableau de données modifiées
        $pdoStatement->execute($donnees);
    }


    public function delete(int $id) :bool
    {
        // la méthode exec retourne le nombre de lignes affectées par la requete
        // ici un seul adherent est ciblé par son id donc une seule ligne sera concernée
        $result = $this->pdo->exec("DELETE FROM Adherent where numero=" . $id);
        // si le traitement (suppression) s'est bien passé la méthode retourne true (1)
        // sinon elle retourne false (0)
        if($result === 1) {
            return true;
        } else {
            return false;
        }
    }
}