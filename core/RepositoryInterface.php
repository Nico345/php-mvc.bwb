<?php

/**
 * Cette interface regroupe toutes les méthodes nécessaires
 * à la récupération de donnéees de la DB.
 * Elle est implémentée à la class DAO
 */
interface RepositoryInterface{

    // cette méthode permet de récupérer toutes les données
    // elle retourne un tableau associatif
    public function getAll();

    // cette méthode permet de récupérer toutes les données par élément(s)
    // elle prend en paramètre un tableau associatif des éléments filtres (clauses WHERE et AND des requetes SQL)
    // elle retourne un tableau associatif
    public function getAllBy(array $filters);
}