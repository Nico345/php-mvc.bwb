<?php

/**
 * Cette interface regroupe toutes les méthodes nécessaires à la création,
 * récupération, mise à jour et suppression de données dans la DB.
 * Elle est implémentée à la class DAO
 * (manipulation d'une seule entité)
 */
interface CRUDInterface {

    // cette méthode permet de créer/insérer des données dans la DB
    // elle prend en paramètre le tableau de données à insérer
    // elle retourne une entité
    public function create(array $data);

    // cette méthode permet de récupérer des données dans la DB
    // elle prend en paramètre l'id du jeu de données à récupérer
    // elle retourne une entité
    public function retrieve(int $id);

    // cette méthode permet de mettre à jour des données dans la DB
    // elle prend en paramètre le tableau de données à mettre à jour
    // elle retourne un booléen, résultat du traitement (échec ou réussite)
    public function update(array $data);

    // cette méthode permet de supprimer des données dans la DB
    // elle prend en paramètre l'id du jeu de données à supprimer
    // elle retourne un booléen, résultat du traitement (échec ou réussite)
    public function delete(int $id);

}
