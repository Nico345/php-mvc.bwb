<?php
/**
 * L'objet Routing va invoquer la méthode définie dans le
 * fichier de configuration séléctionnée grâce à l'URI
 */
class Routing
{
    /**
     * Cette propriété représente les données contenues dans le fichier config/routing.json 
     * sous la forme d'un tableau associatif dont la clé est une URI
     * Elle est initialisée à la construction de l'objet Routing
     */
    private $config;

    /**
     * Cette propriété représente le découpage de l'URI
     * C'est un TABLEAU
     */
    private $uri;

    /**
     * Cette propriété représente le résultat du découpage de la route (clé de config) testée
     * C'est un TABLEAU
     */
    private $route;

    /**
     * Cette propriété correspond au contrôleur qui a été trouvé
     * (valeur correspondant à clé de config dans le tableau URI)
     * C'est un TABLEAU ou une CHAINE DE CARACTERES
     */
    private $controller;

    /**
     * Cette propriété représente les arguments à passer au controller
     * Ce sont les éléments variables de l'URI
     * C'est un TABLEAU
     */
    private $args;

    /**
     * Cette propriété correspond au verbe HTTP utilisé lors de la requête
     * C'est une CHAINE DE CARACTERES
     */
    private $method;


    /**
     * Ce constructeur va initialiser la propriété $config en la peuplant des 
     * données contenues dans le fichier config/routing.json
     */
    public function __construct()
    {
        // je lis le fichier de config avec file_get_contents et je le convertis en tableau associatif
        // avec json_decode (car on a du json au depart) 
        $this->config = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/config/routing.json"), true);
        var_dump($this->config);
        
        // j'explode l'URI courante pour l'obtenir sous forme de tableau $this->uri
        $this->uri = explode("/", $_SERVER['REQUEST_URI']);
        var_dump($this->uri);

        // initialisation du tableau $args à vide
        $this->args = array();

        // initialisation à vide du controller
        $this->controller = "";

        // $keysRoutes = array_keys($this->config);
        // echo "Les clés des routes : <br>";
        // var_dump($keysRoutes);

        // echo "façon 1/ Chaque clé route explode en tableau : <br>";
        // foreach($keysRoutes as $key){
        //     $this->route = explode("/",$key);
        //     var_dump($this->route);
        // }

        // echo "façon 2/ Chaque clé route explode en tableau : <br>";
        // foreach($this->config as $key=>$value){
        //     $this->route = explode("/",$key);
        //     var_dump($this->route);
        // }
    }

    /**
     * Cette méthode déclenche le mécanisme. Elle est donc invoquée à chaque requête HTTP
     * La chaine de caractères de la route est transformée en tableau. Idem pour l'URI.
     * Nous disposons donc de 2 tableaux
     * Elle invoque la méthode compare() si les hauteurs des 2 tableaux correspondent
     */
    public function execute()
    {
        // je stocke les routes de ma config dans une variable sous forme de tableaux
        $keysRoutes = array_keys($this->config);
        foreach($keysRoutes as $key){
            // stockage du tableau de la route courante dans $this->route grace à explode
            $this->route = explode("/",$key);
            // vérif si isEqual est vrai
            if($this->isEqual()) {
                // si la condition est vérifiée j'appelle la méthode compare()
                $this->compare();
            }
        }
        $this->invoke();
    }

    /**
     * Cette méthode vérifie si la hauteur des 2 tableaux est identique ou pas.
     * Le cas échéant elle retourne TRUE sinon elle retournera FALSE
     */
    private function isEqual()
    {
        // je vérifie si la taille des tableaux est égale ou pas
        if(count($this->uri) == count($this->route)){
            return true;
        } else {
            return false;
        }
    }

    /**
     * Cette méthode correspond au contrôleur trouvé
     * Elle retourne la chaine correspondant au contrôleur de la route courante testée :
     *  - soit simplement la valeur de la route (une chaine de caractères)
     *  - soit un tableau associatif dont la clé est la valeur de $_SERVER['REQUEST_METHOD']
     */
    private function getValue()
    {
        // je récupère la methode demandée dans l'URI
        echo "La méthode demandée est :<br>";
        $this->method = $_SERVER['REQUEST_METHOD'];
        var_dump($this->method);

        // je réassemble le tableau de la bonne route en string
        // à ce moment là on connait la clé de notre $config pour laquelle il faut récupérer la valeur dc le bon controller
        $keyRteOK = implode("/", $this->route);
        //echo $keyRteOK;

        // je stocke le bon coltroller dans une variable
        $val = $this->config[$keyRteOK];

        // je vérifie s'il s'agit d'un tableau (ou pas)
        if(is_array($val)) {
            // si oui je boucle dans ce tableau pour obtenir le nom du controller en fonction de la methode (verbe HTTP)
            foreach($val as $key=>$elem) {
                // si la clé est egale à la method alors je retourne le controller associé
                if($key == $this->method) {
                    return $this->controller = $val[$key];
                }
            }
        } // else { ----> pas utile ici en fait
        // s'il ne s'agit pas d'un tableau alors je tombe directement sur le bon controller
        // je n'ai plus qu'à retourner son nom
        return $this->controller = $this->config[$keyRteOK];

        echo "Le nom du contrôleur adéquat est :<br>";
        var_dump($this->controller);
    }

    /**
     * Cette méthode ajoute l'élément variable de l'URI si l'élément en cours est censé être variable
     * Elle vérifie dans le tableau $route si l'élément courant est bien le pattern définissant un élément variable
     * Si oui il faudra ajouter l'élément de l'URI à cet index dans la liste des arguments
     */
    private function addArgument($index)
    {
        // je remplis $args avec la valeur de l'uri où au mm index la valeur de la route est "(:)"
        array_push($this->args, $index);
        echo "<br><br>Tableau $/args :";
        var_dump($this->args);
    }

    /**
     * Cette méthode compare les éléments des 2 tableaux ($uri et $route). S'ils correspondent, la route a donc été trouvée
     * En cas de différence entre 2 éléments, 2 cas de figure :
     *  - soit les éléments sont différents, la méthode retourne FALSE
     *  - soit il s'agit d'un élément variable (signifié par (:)). Dans ce cas l'URI est bonne
     *    Il faut placer la valeur dans la liste des arguments à passer au contrôleur avec la méthode addArgument($index)  
     */
    private function compare()
    {
        echo "tableau de mm taille trouvé :";
        var_dump($this->route);
        // boucle dans tableau uri pour comparer chaque element avec ceux de tableau route courante
        for($i=0 ; $i<count($this->route) ; $i++) {
            echo "test à index " . $i . " :<br>";
            // test si la valeur à l'index de $uri et différente (ou pas) de celle à l'index de $route
            if($this->uri[$i] != $this->route[$i]) {
                // test si le truc différent est egal à (:)
                if($this->route[$i] == "(:)") {
                    echo "la valeur à l'index " . $i . " est un (:) dc va falloir add l'argument !<br>";
                    $this->addArgument($this->uri[$i]);
                }else{
                    echo "la valeur à l'index " . $i . " ne correspond pas !<br>On passe à la route suivante.<br>";
                    return false;
                }
            }
        echo "la valeur à l'index " . $i . " correspond !<br><br>";
        }
        // une fois que j'ai fini de boucler dans mon ou mes tableaux (de taille identique à $uri)
        // j'appelle la méthode getValue() sur l'objet courant
        $this->getValue();
    }

    /**
     * Cette méthode est invoquée quand le contrôleur a été sélectionné
     * Elle crée un objet contrôleur et invoque dynamiquement la méthode du contrôleur adéquat
     * Elle prend en arguments $controlleur et $args
     */
    private function invoke()
    {
        echo "ON RENTRE DANS LE INVOKE<br>";

        // // j'explode mon controller en tableau que je stocke dans une variable
        // // je récupère ainsi le nom de l'objet concret et la méthode associée pour la route correspondant à l'uri
        // $controllerTab = explode(":", $this->controller);
        // var_dump($controllerTab);
        // // je crée un objet concret à partir de la valeur à l'index 0
        // $obj = new $controllerTab[0];
        // echo "Le nom de l'objet est : ". $controllerTab[0];
        // echo "<br>";
        // // je récupère le nom de la méthode que donne la valeur à l'index 1
        // $meth = $controllerTab[1];
        // echo "<br>La méthode est : ".$meth;

        // // j'appelle dynamiquement la méthode sur mon objet, avec les arguments éventuels ($arg)
        // $result = call_user_func_array(array($obj, $meth), $this->args);
        // var_dump($result);

        /**
         * CORRECTION invoke()
         */
        $className = explode(":", $this->controller)[0];
        $methodName = explode(":", $this->controller)[1];

        $object = new $className();

        $result = call_user_func_array(array($object, $methodName), $this->args);
        var_dump($result);
    }
}