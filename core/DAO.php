<?php

/**
 * Cette classe me permet d'interagir avec ma BD Blackbooks
 */
abstract class DAO implements CRUDInterface, RepositoryInterface
{
    // c'est dans cet attribut que sera stocké l'objet PDO
    protected $pdo;

    // cette méthode (le constructeur) permet d'instancier un objet PDO
    public function __construct()
    {
        // ici les parametres sont en dur
        // $this->pdo = new PDO("mysql:dbname=BDD-Blackbooks;host:127.0.0.1", "orel", "root");

        // ici les parametres sont recupérés dans le fichier database.json
        $config = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/config/database.json"),true);
        $dsn = $config['driver'].":dbname=".$config['dbname'].";host=".$config['host'];
        $this->pdo = new PDO($dsn, $config['username'], $config['password']);
    }



}