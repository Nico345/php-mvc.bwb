# php-mvc.bwb

Etude de la POO en PHP à travers la mise en place d’un Framework
basé sur une architecture MVC.
L’objectif est de mettre en œuvre les concepts fondamentaux de la programmation
orientée objets en s’appuyant sur certains patrons de conceptions (design patterns).
